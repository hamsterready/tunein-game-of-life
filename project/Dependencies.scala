import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.2.12"
  lazy val mainArgs = "com.lihaoyi" %% "mainargs" % "0.2.3"
}
