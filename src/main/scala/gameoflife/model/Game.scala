package gameoflife.model

import scala.util.Try

/** Cell, can be dead or alive.
  */
sealed trait Cell

/** Dead cell.
  */
case object Dead extends Cell

/** Alive cell.
  */
case object Alive extends Cell

/** Immutable grid, which is sequence of rows.
  *
  * @param rows sequence of rows
  */
case class Grid(rows: Seq[Row])

/** Immutable row, which is sequence of cells.
  *
  * @param cells sequence of cells
  */
case class Row(cells: Seq[Cell])

/** Game state
  *
  * @param grid grid of cells
  * @param history sequence of grids, useful for inspection and animation
  */
case class Game(grid: Grid, private val history: Seq[Grid])

object Game {

  /** Populate game state using user input.
    *
    * @param w the width of the grid
    * @param h the height of the grid
    * @param cells the set of alive cells
    */
  def apply(w: Int, h: Int, cells: Set[(Int, Int)]): Game = {
    val grid = Grid((0 until h).map { y =>
      Row((0 until w).map(x => if (cells.contains((x, y))) Alive else Dead))
    })

    new Game(grid, Seq.empty)
  }

  /** Creates game state from string. Use `.` for dead and `*` for alive cells.
    *
    * Example:
    * {{{
    * .....**.........
    * ....*..*........
    * ....*.*.........
    * ..**.*...*......
    * .*..*...*.*.....
    * .*.*...*..*.....
    * ..*...*.**......
    * .....*.*........
    * ....*..*........
    * .....**.........
    * }}}
    *
    * @param s input string
    */
  def apply(s: String): Game = {
    Game(
      Grid(s.split("\n").map(_.split("").map(c => if (c == ".") Dead else Alive).toList).map(Row(_)).toList),
      List.empty
    )
  }

  /** Show game with basic stats.
    *
    * @param g the game to show
    */
  private def show(g: Game): String = {
    val grid = g.grid.rows
      .map(
        _.cells
          .map {
            case Alive => "*"
            case Dead  => "."
          }
          .mkString("    ", "", "")
      )
      .mkString("\n")

    val aliveCount = g.grid.rows.map(_.cells.count(_ == Alive)).sum
    val deadCount = g.grid.rows.map(_.cells.length).sum - aliveCount

    val header = s"Game of Life ========= generation ${g.history.length}"
    val stats = s"Alive: $aliveCount, dead: $deadCount"
    val separator = "==================================="

    s"""
       |$header
       |$stats
       |
       |$grid
       |
       |$separator""".stripMargin
  }

  /** Help for the UI
    */
  private val help: String =
    """
      |Press enter to run next iteration.
      |Press anything else followed by enter to quit.
      |""".stripMargin

  /** Main game loop. Prints current state, run simulation step and asks for user input.
    * @param g the state to step
    */
  def loop(g: Game): Unit = {
    println(Game.show(g))
    println(Game.help)
    val updated = step(g)
    val c = Console.in.read.toChar
    if (c == '\n') loop(updated) else ()
  }

  /** From the provided requirements:
    *
    * At each step in time, the following transitions occur:
    *  1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
    *  2. Any live cell with two or three live neighbours lives on to the next generation.
    *  3. Any live cell with more than three live neighbours dies, as if by overcrowding.
    *  4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    *
    * @param g the game to step
    */
  private[gameoflife] def step(g: Game): Game = {
    def getCell(x: Int, y: Int): Option[Cell] = {
      Try(g.grid.rows(y).cells(x)).toOption // not most optimal
    }

    def aliveInNeighborhood(x: Int, y: Int): Int = {
      val r = -1 to 1
      val cellsAround = for {
        yr <- r
        xr <- r
      } yield if (xr == 0 && yr == 0) None else getCell(x + xr, y + yr)

      cellsAround.collect({ case Some(Alive) => 1 }).sum
    }

    val grid = Grid(
      g.grid.rows.zipWithIndex
        .map { case (r, y) =>
          r.cells.zipWithIndex.map { case (c, x) =>
            val n = aliveInNeighborhood(x, y)
            c match {
              case Alive if n < 2            => Dead
              case Alive if n >= 2 && n <= 3 => Alive
              case Alive if n > 3            => Dead
              case Dead if n == 3            => Alive
              case Dead                      => Dead
              case Alive                     => Dead
            }
          }
        }
        .map(Row(_))
    )

    g.copy(grid = grid, history = g.history :+ grid)
  }

}
