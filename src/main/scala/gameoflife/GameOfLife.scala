package gameoflife
import gameoflife.model.Game
import mainargs.{ParserForMethods, arg, main}

import scala.util.{Success, Try}

/** Game of Life application.
  *
  * Example command line arguments to use:
  * {{{
  *   # glider
  *   sbt "run -w 25 -h 25 -a 12,12 -a 13,13 -a 11,14 -a 12,14 -a 13,14"
  *   # oscillator
  *   sbt "run -w 5 -h 5 -a 1,2 -a 2,2 -a 3,2"
  *   # F-pentomino
  *   sbt "run -w 30 -h 30 -a 15,15 -a 16,15 -a 15,16 -a 14,16 -a 15,17"
  *   # acorn
  *   sbt "run -w 40 -h 40 -a 21,19 -a 23,20 -a 20,21 -a 21,21 -a 24,21 -a 25,21 -a 26,21"
  * }}}
  */
object GameOfLife {

  @main
  def run(
      @arg(short = 'w', doc = "Width of the grid") w: Int = 25,
      @arg(name = "h", doc = "Height of the grid") h: Int = 25,
      @arg(doc = "List of alive cells locations, use like this: -a x1,y1 -a x2,y2 ...") a: List[String]
  ): Unit = {
    // parse input
    val alive = a
      .map(_.split(","))
      .map {
        case Array(x, y) =>
          (for {
            xInt <- Try(x.toInt)
            yInt <- Try(y.toInt)
          } yield (xInt, yInt)) match {
            case Success((xInt, yInt)) if xInt >= 0 && xInt < w && yInt >= 0 && yInt < h =>
              (xInt, yInt)
            case e @ _ =>
              throw new IllegalArgumentException(
                s"Arguments for -a must be integer and match grid dimension ${w}x$h. Offending value $e"
              )
          }
        case arg @ _ =>
          throw new IllegalArgumentException(s"Invalid argument $arg for -a")
      }
      .toSet

    // create game and loop
    Game.loop(Game(w, h, alive))
  }

  def main(args: Array[String]): Unit = {
    ParserForMethods(this).runOrExit(args.toSeq)
    ()
  }

}
