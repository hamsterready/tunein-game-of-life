package gameoflife

import gameoflife.model.{Alive, Dead, Game}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class GameSpec extends AnyFlatSpec with should.Matchers {
  behavior of ("Game")

  it should "populate cells properly while constructing" in {
    val g = Game(3, 3, Set((1, 1), (2, 1)))
    g.grid.rows.length should equal(3)
    g.grid.rows.foreach(_.cells.length should equal(3))
    g.grid.rows(1).cells(1) should be(Alive)
    g.grid.rows(1).cells(2) should be(Alive)

    val cords = for {
      x <- 0 until 3
      y <- 0 until 3
    } yield (x, y)

    cords
      .filterNot(_ != (1, 1))
      .filterNot(_ != (2, 1))
      .foreach { case (x, y) => g.grid.rows(y).cells(x) should be(Dead) }
  }

  it should "populate cells properly from string" in {
    val g = Game(
      """|*..
         |.*.
         |...
         |""".stripMargin
    )
    g.grid.rows(0).cells(0) should be(Alive)
    g.grid.rows(1).cells(1) should be(Alive)

    val cords = for {
      x <- 0 until 3
      y <- 0 until 3
    } yield (x, y)

    cords
      .filterNot(_ != (0, 0))
      .filterNot(_ != (1, 1))
      .foreach { case (x, y) => g.grid.rows(y).cells(x) should be(Dead) }
  }

  /** 1. Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
    */
  it should "handle cells with fewer than two live neighbours" in {
    val g = Game(
      """|*..
         |...
         |..*
         |""".stripMargin
    )
    val n = Game.step(g)

    val e = Game(
      """|...
         |...
         |...
         |""".stripMargin
    )

    n.grid should equal(e.grid)
  }

  /** 2. Any live cell with two or three live neighbours lives on to the next generation.
    */
  it should "handle cells with two or three live neighbours" in {
    val g = Game(
      """|...
         |**.
         |**.
         |""".stripMargin
    )
    val n = Game.step(g)

    val e = Game(
      """|...
         |**.
         |**.
         |""".stripMargin
    )

    n.grid should equal(e.grid)
  }

  /** 3. Any live cell with more than three live neighbours dies, as if by overcrowding.
    */
  it should "handle cells with more than three live neighbours" in {
    val g = Game(
      """|...
         |***
         |**.
         |""".stripMargin
    )
    val n = Game.step(g)

    val e = Game(
      """|.*.
         |*.*
         |*.*
         |""".stripMargin
    )

    n.grid should equal(e.grid)
  }

  /** 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
    */
  it should "handle dead cells with exactly three live neighbours" in {
    val g = Game(
      """|...
         |*.*
         |.*.
         |""".stripMargin
    )
    val n = Game.step(g)

    val e = Game(
      """|...
         |.*.
         |.*.
         |""".stripMargin
    )

    n.grid should equal(e.grid)
  }

}
