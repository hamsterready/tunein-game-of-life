# TuneIn Game of Life Exercise

See [PDF](./TuneIn - Game of Life exercise.pdf) for problem description.

## How to run?
If you trust this code and have sbt installed just run:

```shell
sbt run --help
# glider
sbt "run -w 25 -h 25 -a 12,12 -a 13,13 -a 11,14 -a 12,14 -a 13,14"
# oscillator
sbt "run -w 5 -h 5 -a 1,2 -a 2,2 -a 3,2"
# F-pentomino
sbt "run -w 30 -h 30 -a 15,15 -a 16,15 -a 15,16 -a 14,16 -a 15,17"
# acorn
sbt "run -w 40 -h 40 -a 21,19 -a 23,20 -a 20,21 -a 21,21 -a 24,21 -a 25,21 -a 26,21"
```

If you do not trust the code use docker:

```
docker build . -t gol-maciej
docker run -it gol-maciej
# acorn
docker run -it gol-maciej sbt "run -w 40 -h 40 -a 21,19 -a 23,20 -a 20,21 -a 21,21 -a 24,21 -a 25,21 -a 26,21"
```

## To run tests

```shell
sbt test
```
