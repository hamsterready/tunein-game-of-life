FROM hseeberger/scala-sbt:graalvm-ce-21.3.0-java17_1.6.2_3.1.1

COPY ./ /app

WORKDIR /app

RUN sbt compile
CMD sbt "run -w 25 -h 25 -a 12,12 -a 13,13 -a 11,14 -a 12,14 -a 13,14"
